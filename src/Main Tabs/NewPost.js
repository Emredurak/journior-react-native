import React from 'react';
import {Text, StyleSheet, View} from 'react-native';

const Feed = () => {
  return (
    <View>
      <Text style={styles.header}>New Post</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    fontSize: 20,
  },
});

export default Feed;
