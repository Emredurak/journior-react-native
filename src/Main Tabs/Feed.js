import React from 'react';
import {StyleSheet, FlatList, View} from 'react-native';
import AgencyNews from '../components/AgencyNews';
import dimensions from '../styles/dimensions';
import colors from '../styles/colors';

const images = [
  'https://cdn.pixabay.com/photo/2015/03/26/09/47/sky-690293__340.jpg',
  'https://cdn.pixabay.com/photo/2015/11/04/20/59/milky-way-1023340_960_720.jpg',
  'https://cdn.pixabay.com/photo/2016/03/09/09/22/meeting-1245776_960_720.jpg',
  'https://cdn.pixabay.com/photo/2015/06/08/15/11/typewriter-801921_960_720.jpg',
  'https://cdn.pixabay.com/photo/2015/03/17/14/05/sparkler-677774_960_720.jpg',
  'https://cdn.pixabay.com/photo/2014/05/03/00/56/summerfield-336672_960_720.jpg',
  'https://cdn.pixabay.com/photo/2014/12/15/17/16/boardwalk-569314_960_720.jpg',
  'https://cdn.pixabay.com/photo/2016/02/19/11/36/microphone-1209816_960_720.jpg',
  'https://cdn.pixabay.com/photo/2015/07/27/19/47/turtle-863336_960_720.jpg',
  'https://cdn.pixabay.com/photo/2016/03/09/10/37/light-1246043_960_720.jpg',
];
const news = [];

for (let i = 0; i < 10; i++) {
  let item = {
    key: '' + i,
    id: i,
    header: 'Test Header some long long text header lorem ipsum' + i,
    subheader:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum." +
      i,
    src: images[i],
    liked: false,
    newsUrl: 'https://journior.com',
  };
  news.push(item);
}

const Feed = ({navigation, route}) => {
  const flatListRef = React.useRef();
  if (route.params) {
    if (route.params.scrollToTop) {
      flatListRef.current.scrollToOffset({animated: true, offset: 0});
    }
  }

  return (
    <FlatList
      ref={flatListRef}
      showsVerticalScrollIndicator={false}
      data={news}
      renderItem={({item}) => {
        return (
          <View style={styles.itemContainer}>
            <AgencyNews
              header={item.header}
              subheader={item.subheader}
              src={item.src}
              liked={item.liked}
              newsUrl={item.newsUrl}
              navigation={navigation}
            />
          </View>
        );
      }}
    />
  );
};

const styles = StyleSheet.create({
  itemContainer: {
    paddingBottom: dimensions.bigPadding,
    paddingTop: dimensions.xxShortPadding,
    borderBottomWidth: 1,
    borderColor: colors.separatorColor,
    backgroundColor: colors.background,
  },
});

export default Feed;
