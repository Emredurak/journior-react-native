import React from 'react';
import {Text, StyleSheet, View} from 'react-native';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faStar} from '@fortawesome/free-solid-svg-icons';

const Feed = () => {
  return (
    <View>
      <Text style={styles.header}>Browse</Text>
      <FontAwesomeIcon icon={faStar} />
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    fontSize: 24,
  },
});

export default Feed;
