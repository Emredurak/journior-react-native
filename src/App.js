import 'react-native-gesture-handler';
import * as React from 'react';
import {StyleSheet, Platform} from 'react-native';
import {NavigationContainer, DefaultTheme} from '@react-navigation/native';
import {createStackNavigator, TransitionPresets} from '@react-navigation/stack';
import colors from './styles/colors';
import NewsWebView from './components/NewsWebView';
import Test from './components/Test';
import Sidebar from './components/Sidebar';
import Login from './components/Login';
import MyProfile from './components/MyProfile';
import {enableScreens} from 'react-native-screens';
import RNBootSplash from 'react-native-bootsplash';

const Stack = createStackNavigator();
const MyTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    background: colors.background,
  },
};
function App() {
  enableScreens();
  return (
    <NavigationContainer
      theme={MyTheme}
      onReady={() => RNBootSplash.hide({fade: true})}>
      <Stack.Navigator
        initialRouteName="Login"
        screenOptions={{
          ...TransitionPresets.SlideFromRightIOS,
          gestureEnabled: false,
          headerTitleAlign: 'center',
          headerTitleStyle: {
            color: colors.textPrimary,
            textTransform: 'uppercase',
          },
        }}>
        <Stack.Screen
          name="Sidebar"
          component={Sidebar}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="Login"
          component={Login}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="NewsWebView"
          component={NewsWebView}
          options={{title: 'Jr'}}
        />
        <Stack.Screen name="Test" component={Test} />
        <Stack.Screen name="MyProfile" component={MyProfile} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  margin: {
    ...Platform.select({
      android: {
        margin: 16,
      },
    }),
  },
});

export default App;
