
const colors = {
  background: '#ffffff',
  textPrimary: '#000000ff',
  textSecondary: '#8f8f8f',
  error: '#ED473F',
  accentBurgundy: '#9b002b',
  accentBurgundyLight: '#9b002b60',
  separatorColor: '#e0e0e0',
  mdTextFieldTintColor: '#000000ff',
};

export default colors;
