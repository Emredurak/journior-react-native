import dimensions from './dimensions';
import colors from './colors';

const style_variables = {
  standardFontFamily: 'NotoSans-Regular',
  boldFontFamily: 'NotoSans-Bold',
  buttonFontFamily: 'PTSans-Regular',
  buttonBoldFontFamily: 'PTSans-Bold',
  quoteFont: 'PlayfairDisplay-VariableFont_wght',
  mainSubtitleFont: 'RobotoCondensed-Regular',

  headerIcon: {
    margin: dimensions.bigPadding,
  },
  blackButton: {
    backgroundColor: colors.textPrimary,
    color: colors.background,
  },
  buttonOutlineBurgundy: {
    alignSelf: 'center',
    textAlign: 'center',
    backgroundColor: colors.background,
    color: colors.accentBurgundy,
    borderWidth: 1,
    borderColor: colors.accentBurgundy,
    textTransform: 'uppercase',
  },

  // Agency News
  title: {
    fontFamily: 'NotoSans-Bold',
    fontSize: dimensions.headerFontSize,
    //fontWeight: 'bold',
    textAlignVertical: 'top',
    flex: 1,
    paddingRight: dimensions.xShortPadding,
  },
  subtitle: {
    marginHorizontal: 12,
    marginVertical: 8,
    paddingHorizontal: 16,
    paddingVertical: 8,
    borderLeftWidth: 2,
    borderColor: colors.accentBurgundy,
  },
  showMore: {
    width: 30,
    height: 30,
    padding: dimensions.xShortPadding,
  },
};

export default style_variables;
