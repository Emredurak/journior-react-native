import {Dimensions} from 'react-native';

const dimensions = {
  // device
  maxWidth: Dimensions.get('window').width,

  // Margins
  bigMargin: 16,
  standardMargin: 10,
  shortMargin: 8,
  xShortMargin: 4,

  // Paddings
  bigPadding: 16,
  standardPadding: 12,
  shortPadding: 10,
  xShortPadding: 8,
  xxShortPadding: 4,

  //Border Definitions
  standardBorderRadius: 8,
  buttonBorderWidth: 2,

  //Font Sizes
  headerFontSize: 18,
  headerDetailFontSize: 35,
  biggestFontSize: 48,
  bigFontSize: 20,
  standardFontSize: 16,
  shortFontSize: 13,
  xShortFontSize: 11,
  xxShortFontSize: 9,

  //Agency News
  subtextMaxLine: 6,

  // Profile Section
  ppSize: 120,
};

export default dimensions;
