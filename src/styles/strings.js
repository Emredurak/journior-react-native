const Strings = {
  app_name: 'Journior',
  following_list: 'Following List',
  subscriber: 'Subscriber',
  subscribers: 'Subscribers',
  subscription: 'Subscription',
  subscriptions: 'Subscriptions',
  post: 'Post',
  posts: 'Posts',
  policy: 'Journalism Policy',
  settings: 'Settings',
  loginNote: 'We are happy to see you!',
  login: 'LOGIN',
  register: 'REGISTER',
  exit: 'EXIT',
};

export default Strings;
