import 'react-native-gesture-handler';
import * as React from 'react';
import {TransitionPresets} from '@react-navigation/stack';

import HomeScreen from './HomeScreen';
import {createDrawerNavigator} from '@react-navigation/drawer';
import DrawerContent from './DrawerContent';
import colors from '../styles/colors';
import {useEffect, useState} from 'react';
import {ActivityIndicator, InteractionManager} from 'react-native';
import strings from "../styles/strings";

const Drawer = createDrawerNavigator();

function Sidebar() {
  const [interactionsComplete, setInteractionsComplete] = useState(false);
  useEffect(() => {
    InteractionManager.runAfterInteractions(() => {
      setInteractionsComplete(true);
    });
  }, []); // passing an empty array as second argument triggers the callback in useEffect only after the initial render thus replicating `componentDidMount` lifecycle behaviour
  if (!interactionsComplete) {
    return (
      <ActivityIndicator
        style={styles.activityIndicator}
        size={'large'}
        color={colors.textPrimary}
      />
    );
  }

  return (
    <Drawer.Navigator
      initialRouteName="Home"
      screenOptions={{
        ...TransitionPresets.SlideFromRightIOS,
        gestureEnabled: true,
        headerShown: true,
        headerTitleAlign: 'center',
        headerTitleStyle: {
          color: colors.textPrimary,
          textTransform: 'uppercase',
        },
      }}
      drawerContent={props => <DrawerContent {...props} />}>
      <Drawer.Screen
        name={strings.app_name}
        component={HomeScreen}
        options={{
          gestureEnabled: true,
        }}
      />
    </Drawer.Navigator>
  );
}

const styles = {
  activityIndicator: {
    alignSelf: 'center',
    flex: 1,
  },
};

export default Sidebar;
