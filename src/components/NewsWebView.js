import React from 'react';
import {WebView} from 'react-native-webview';

const NewsWebView = ({route}) => {
  return <WebView source={{uri: route.params.src}} />;
};

export default NewsWebView;
