import * as React from 'react';
import {memo, useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Image,
  Text,
  TouchableOpacity,
  InteractionManager,
  ActivityIndicator,
} from 'react-native';
import {SceneMap, TabBar} from 'react-native-tab-view';
import {HFlatList} from 'react-native-head-tab-view';
import {CollapsibleHeaderTabView} from 'react-native-tab-view-collapsible-header';
import {faEdit} from '@fortawesome/free-regular-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import dimensions from '../styles/dimensions';
import strings from '../styles/strings';
import colors from '../styles/colors';
import AgencyNews from './AgencyNews';
import app_variables from '../styles/app_variables';

const images = [
  'https://cdn.pixabay.com/photo/2015/03/26/09/47/sky-690293__340.jpg',
  'https://cdn.pixabay.com/photo/2015/11/04/20/59/milky-way-1023340_960_720.jpg',
  'https://cdn.pixabay.com/photo/2016/03/09/09/22/meeting-1245776_960_720.jpg',
  'https://cdn.pixabay.com/photo/2015/06/08/15/11/typewriter-801921_960_720.jpg',
  'https://cdn.pixabay.com/photo/2015/03/17/14/05/sparkler-677774_960_720.jpg',
  'https://cdn.pixabay.com/photo/2014/05/03/00/56/summerfield-336672_960_720.jpg',
  'https://cdn.pixabay.com/photo/2014/12/15/17/16/boardwalk-569314_960_720.jpg',
  'https://cdn.pixabay.com/photo/2016/02/19/11/36/microphone-1209816_960_720.jpg',
  'https://cdn.pixabay.com/photo/2015/07/27/19/47/turtle-863336_960_720.jpg',
  'https://cdn.pixabay.com/photo/2016/03/09/10/37/light-1246043_960_720.jpg',
];
const news = [];

for (let i = 0; i < 10; i++) {
  let item = {
    id: i,
    header: 'Test Header some long long text header lorem ipsum' + i,
    subheader:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum." +
      i,
    src: images[i],
    liked: false,
    newsUrl: 'https://journior.com',
  };
  news.push(item);
}

const FirstRoute = ({route}) => {
  let renderItem = ({item}) => (
    <View style={styles.itemContainer}>
      <AgencyNews
        header={item.header}
        subheader={item.subheader}
        src={item.src}
        liked={item.liked}
        newsUrl={item.newsUrl}
        navigation={route.navigation}
      />
    </View>
  );

  return (
    <HFlatList
      index={0}
      showsVerticalScrollIndicator={false}
      data={news}
      keyExtractor={({item}, index) => index.toString()}
      renderItem={renderItem}
    />
  );
};

const SecondRoute = ({route}) => {
  let renderItem = ({item}) => (
    <View style={styles.itemContainer}>
      <AgencyNews
        header={item.header}
        subheader={item.subheader}
        src={item.src}
        liked={item.liked}
        newsUrl={item.newsUrl}
        navigation={route.navigation}
      />
    </View>
  );

  return (
    <HFlatList
      index={1}
      showsVerticalScrollIndicator={false}
      data={news}
      keyExtractor={({item}, index) => index.toString()}
      renderItem={renderItem}
    />
  );
};
function addItem() {
  let item = {
    id: 30,
    header: 'Test Header some long long text header lorem ipsum' + 30,
    subheader:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum." +
      30,
    src: images[0],
    liked: false,
    newsUrl: 'https://journior.com',
  };
  news.unshift(item);
}

const initialLayout = {width: Dimensions.get('window').width};

const MyProfile = ({navigation}) => {
  const [interactionsComplete, setInteractionsComplete] = useState(false);
  useEffect(() => {
    InteractionManager.runAfterInteractions(() => {
      setInteractionsComplete(true);
    });
    console.log('mount it!');
  }, []); // passing an empty array as second argument triggers the callback in useEffect only after the initial render thus replicating `componentDidMount` lifecycle behaviour

  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <FontAwesomeIcon
          icon={faEdit}
          size={20}
          style={app_variables.headerIcon}
          onPress={() => navigation.navigate('Test')}
        />
      ),
    });
  }, [navigation]);

  let profile = {
    fullName: 'Emre Durak',
    username: 'mr.drk94',
    location: 'İstanbul',
    bio: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
    subscriberCount: 1,
    subscriptionCount: 24,
    postCount: 13,
  };

  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    {key: 'first', title: 'Posts', navigation: navigation},
    {key: 'second', title: 'Saved', navigation: navigation},
  ]);

  const renderScene = SceneMap({
    first: FirstRoute,
    second: SecondRoute,
  });

  if (!interactionsComplete) {
    return (
      <ActivityIndicator
        style={styles.activityIndicator}
        size={'large'}
        color={colors.textPrimary}
      />
    );
  }

  return (
    <CollapsibleHeaderTabView
      renderScrollHeader={() => (
        <View style={styles.profileContainer}>
          <Image
            source={{uri: 'https://reactjs.org/logo-og.png'}}
            style={styles.profilePhoto}
          />
          <Text style={styles.fullName}>{profile.fullName}</Text>
          <Text style={styles.username}>{profile.username}</Text>
          <Text style={styles.location}>{profile.location}</Text>
          <Text numberOfLines={4} style={styles.bio}>
            {profile.bio.trim()}
          </Text>
          <View style={styles.buttonContainer}>
            <TouchableOpacity
              onPress={() => {
                addItem();
              }}>
              <View>
                <Text style={styles.buttonValue}>
                  {profile.subscriberCount}
                </Text>
                <Text style={styles.buttonLabel}>{strings.subscribers}</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity>
              <View>
                <Text style={styles.buttonValue}>
                  {profile.subscriptionCount}
                </Text>
                <Text style={styles.buttonLabel}>{strings.subscriptions}</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity>
              <View>
                <Text style={styles.buttonValue}>{profile.postCount}</Text>
                <Text style={styles.buttonLabel}>{strings.posts}</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      )}
      navigationState={{index, routes}}
      renderScene={renderScene}
      onIndexChange={setIndex}
      initialLayout={initialLayout}
      renderTabBar={props => (
        <TabBar
          {...props}
          style={styles.tabBar}
          activeColor={colors.textPrimary}
          inactiveColor={colors.textSecondary}
          indicatorStyle={styles.tabBarIndicator}
          pressColor={colors.background}
          labelStyle={styles.tabLabel}
        />
      )}
    />
  );
};

const styles = StyleSheet.create({
  scene: {
    flex: 1,
  },
  profileContainer: {
    alignItems: 'center',
    backgroundColor: colors.background,
    paddingBottom: dimensions.bigPadding,
  },
  profilePhoto: {
    width: dimensions.ppSize,
    height: dimensions.ppSize,
    borderRadius: dimensions.ppSize,
    marginVertical: dimensions.standardMargin,
  },
  fullName: {
    fontSize: dimensions.standardFontSize,
  },
  username: {
    fontSize: dimensions.shortFontSize,
    color: colors.textSecondary,
  },
  location: {
    fontSize: dimensions.shortFontSize,
  },
  bio: {
    margin: dimensions.standardMargin,
    paddingHorizontal: dimensions.standardPadding,
    paddingBottom: dimensions.standardPadding,
    fontSize: dimensions.shortFontSize,
  },
  buttonContainer: {
    width: '100%',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingBottom: dimensions.bigPadding,
    marginBottom: dimensions.standardMargin,
  },
  buttonValue: {
    textAlign: 'center',
    fontSize: dimensions.standardFontSize,
    fontWeight: 'bold',
  },
  buttonLabel: {
    textAlign: 'center',
    color: colors.textSecondary,
    fontSize: dimensions.shortFontSize,
  },
  tabLabel: {
    fontSize: dimensions.shortFontSize,
  },
  tabBar: {
    backgroundColor: colors.background,
    elevation: 0,
    marginBottom: 0,
  },
  tabBarIndicator: {
    backgroundColor: colors.textPrimary,
    height: 1,
  },
  activityIndicator: {
    alignSelf: 'center',
    flex: 1,
  },
});

export default memo(MyProfile);
