// React Navigation Drawer with Sectioned Menu Options & Footer
// https://aboutreact.com/navigation-drawer-sidebar-menu-with-sectioned-menu-options-footer/

import React from 'react';
import {
  FlatList,
  Image,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableNativeFeedback,
  View,
} from 'react-native';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';

import dimensions from '../styles/dimensions';
import colors from '../styles/colors';
import strings from '../styles/strings';
import {faFileAlt, faSun} from '@fortawesome/free-regular-svg-icons';

const images = [
  'https://cdn.pixabay.com/photo/2015/03/26/09/47/sky-690293__340.jpg',
  'https://cdn.pixabay.com/photo/2015/11/04/20/59/milky-way-1023340_960_720.jpg',
  'https://cdn.pixabay.com/photo/2016/03/09/09/22/meeting-1245776_960_720.jpg',
  'https://cdn.pixabay.com/photo/2015/06/08/15/11/typewriter-801921_960_720.jpg',
  'https://cdn.pixabay.com/photo/2015/03/17/14/05/sparkler-677774_960_720.jpg',
  'https://cdn.pixabay.com/photo/2014/05/03/00/56/summerfield-336672_960_720.jpg',
  'https://cdn.pixabay.com/photo/2014/12/15/17/16/boardwalk-569314_960_720.jpg',
  'https://cdn.pixabay.com/photo/2016/02/19/11/36/microphone-1209816_960_720.jpg',
  'https://cdn.pixabay.com/photo/2015/07/27/19/47/turtle-863336_960_720.jpg',
  'https://cdn.pixabay.com/photo/2016/03/09/10/37/light-1246043_960_720.jpg',
];
const followingList = [];

for (let i = 0; i < 5; i++) {
  let item = {
    key: '' + i,
    id: i,
    username: 'username' + i,
    name: 'Test User ' + i,
    subscriberCount: i,
    imgSrc: images[i],
  };
  followingList.push(item);
}

const followingItem = (imgSrc, name, subscriberCount, username, id) => {
  let subscriberLabel =
    subscriberCount > 1 ? strings.subscribers : strings.subscriber;
  return (
    <TouchableNativeFeedback>
      <View style={styles.itemProfile}>
        <Image source={{uri: imgSrc}} style={styles.itemProfilePhoto} />
        <View style={styles.itemProfileInfo}>
          <Text style={styles.itemProfileName}>{name}</Text>
          <Text style={styles.itemSubscribers}>
            {subscriberCount} {subscriberLabel}
          </Text>
        </View>
      </View>
    </TouchableNativeFeedback>
  );
};

const footerItem = (icon, label) => {
  return (
    <TouchableNativeFeedback>
      <View style={styles.footerItem}>
        <FontAwesomeIcon
          icon={icon}
          size={dimensions.standardFontSize}
          style={styles.footerItemIcon}
        />
        <Text style={styles.footerItemText}> {label} </Text>
      </View>
    </TouchableNativeFeedback>
  );
};
const DrawerContent = ({navigation}) => {
  return (
    <SafeAreaView style={styles.container}>
      <TouchableNativeFeedback
        onPress={() => {
          navigation.navigate('MyProfile');
        }}>
        <View style={styles.profile}>
          <Image
            source={{uri: 'https://reactjs.org/logo-og.png'}}
            style={styles.profilePhoto}
          />
          <View style={styles.profileInfo}>
            <Text style={styles.profileName}>Emre Durak</Text>
            <Text style={styles.subscribers}>2 Subscribers</Text>
          </View>
        </View>
      </TouchableNativeFeedback>
      <Text style={styles.followingListLabel}>{strings.following_list}</Text>
      <FlatList
        showsVerticalScrollIndicator={false}
        data={followingList}
        renderItem={({item}) => {
          return followingItem(
            item.imgSrc,
            item.name,
            item.subscriberCount,
            item.username,
            item.id,
          );
        }}
      />
      <View>
        {footerItem(faFileAlt, strings.policy)}
        {footerItem(faSun, strings.settings)}
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  // Profile
  profile: {
    paddingTop: dimensions.bigPadding * 2,
    padding: dimensions.bigPadding,
    flexDirection: 'row',
  },
  profilePhoto: {
    width: 75,
    height: 75,
    borderRadius: 75,
  },
  profileInfo: {
    marginLeft: dimensions.standardMargin,
    justifyContent: 'center',
  },
  profileName: {
    color: colors.textPrimary,
    fontSize: dimensions.standardFontSize,
  },
  subscribers: {
    color: colors.textSecondary,
    fontSize: dimensions.shortFontSize,
  },

  // Followings
  followingListLabel: {
    color: colors.textPrimary,
    textAlign: 'right',
    padding: dimensions.standardPadding,
    fontWeight: 'bold',
  },
  itemProfile: {
    paddingHorizontal: dimensions.bigPadding,
    paddingVertical: dimensions.xShortPadding,
    flexDirection: 'row',
  },
  itemProfilePhoto: {
    width: 40,
    height: 40,
    borderRadius: 40,
  },
  itemProfileInfo: {
    marginLeft: dimensions.standardMargin,
    justifyContent: 'center',
  },
  itemProfileName: {
    color: colors.textPrimary,
    fontSize: dimensions.shortFontSize,
  },
  itemSubscribers: {
    color: colors.textSecondary,
    fontSize: dimensions.xShortFontSize,
  },

  // Footer
  footerItem: {
    flexDirection: 'row',
    padding: dimensions.standardPadding,
    borderTopWidth: 1,
    borderColor: colors.separatorColor,
    backgroundColor: colors.background,
  },
  footerItemIcon: {alignSelf: 'center'},
  footerItemText: {
    alignSelf: 'center',
    paddingLeft: dimensions.shortPadding,
    fontSize: dimensions.shortFontSize,
    color: colors.textPrimary,
  },
});
export default DrawerContent;
