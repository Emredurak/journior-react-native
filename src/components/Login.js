import React, {useState} from 'react';
import {
  View,
  TouchableNativeFeedback,
  useWindowDimensions,
  Image,
  StyleSheet,
  Text,
  ScrollView,
} from 'react-native';

import {Button} from 'react-native-elements';
import {
  TextField,
  FilledTextField,
  OutlinedTextField,
} from 'rn-material-ui-textfield';

import {TabView, SceneMap} from 'react-native-tab-view';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import dimensions from '../styles/dimensions';
import strings from '../styles/strings';
import colors from '../styles/colors';
import {faUser} from '@fortawesome/free-regular-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import app_variables from '../styles/app_variables';

const confirmRef = React.createRef();

let user = {
  username: '',
  password: '',
  confirm: '',
};

// Login Screen
const FirstRoute = props => {
  function loginSuccessful() {
    props.route.navigation.replace('Sidebar');
  }
  const usernameRef = React.createRef();
  const passwordRef = React.createRef();
  const [passwordHidden, hidePassword] = useState(true);

  function onUsernameChange() {
    let {current: field} = usernameRef;
    user.username = field.value();
  }
  function onPasswordChange() {
    let {current: field} = passwordRef;
    user.password = field.value();
  }
  return (
    <View style={styles.tabContainer}>
      <BrandHeader></BrandHeader>
      <OutlinedTextField
        label="Username or E-mail"
        keyboardType="email-address"
        ref={usernameRef}
        onChangeText={onUsernameChange}
        autoCorrect={false}
        tintColor={colors.mdTextFieldTintColor}
        characterRestriction={32}
      />
      <OutlinedTextField
        ref={passwordRef}
        secureTextEntry={passwordHidden}
        autoCapitalize="none"
        autoCorrect={false}
        tintColor={colors.mdTextFieldTintColor}
        enablesReturnKeyAutomatically={true}
        onChangeText={onPasswordChange}
        returnKeyType="done"
        label="Password"
        maxLength={30}
        characterRestriction={20}
        containerStyle={styles.textFieldContainer}
      />
      <Button
        title={strings.login}
        type="solid"
        raised
        containerStyle={styles.loginButton}
        buttonStyle={[app_variables.blackButton]}
        titleStyle={{textTransform: 'uppercase'}}
        onPress={loginSuccessful}
      />
      <TouchableNativeFeedback
        onPress={() => {
          props.jumpTo('second');
        }}>
        <Text style={styles.bottomText}>Do you have an account?</Text>
      </TouchableNativeFeedback>
    </View>
  );
};

// Register Screen
const SecondRoute = props => {
  const usernameRegisterRef = React.createRef();
  const passwordRegisterRef = React.createRef();
  const confirmRef = React.createRef();
  const [passwordHidden, hidePassword] = useState(true);

  function onUsernameChange() {
    let {current: field} = usernameRegisterRef;
    user.username = field.value();
  }
  function onPasswordChange() {
    let {current: field} = passwordRegisterRef;
    user.password = field.value();
  }
  return (
    <View style={styles.tabContainer}>
      <BrandHeader/>
      <OutlinedTextField
        label="Username or E-mail"
        keyboardType="email-address"
        ref={usernameRegisterRef}
        onChangeText={onUsernameChange}
        autoCorrect={false}
        tintColor={colors.mdTextFieldTintColor}
      />
      <OutlinedTextField
        ref={passwordRegisterRef}
        secureTextEntry={passwordHidden}
        autoCapitalize="none"
        autoCorrect={false}
        tintColor={colors.mdTextFieldTintColor}
        enablesReturnKeyAutomatically={true}
        onChangeText={onPasswordChange}
        returnKeyType="done"
        label="Password"
        title="8-16 Characters, Uppercase and lowercase letters"
        maxLength={30}
        characterRestriction={20}
        containerStyle={styles.textFieldContainer}
      />
      <OutlinedTextField
        ref={confirmRef}
        secureTextEntry={passwordHidden}
        autoCapitalize="none"
        autoCorrect={false}
        tintColor={colors.mdTextFieldTintColor}
        enablesReturnKeyAutomatically={true}
        onChangeText={onPasswordChange}
        returnKeyType="done"
        label="Confirm"
        maxLength={30}
        characterRestriction={20}
        containerStyle={styles.textFieldContainer}
      />
      <Button
        title={strings.register}
        type="solid"
        raised
        containerStyle={styles.loginButton}
        buttonStyle={[app_variables.blackButton]}
        titleStyle={{textTransform: 'uppercase'}}
      />
      <TouchableNativeFeedback
        onPress={() => {
          props.jumpTo('first');
        }}>
        <Text style={styles.bottomText}>Already have an account?</Text>
      </TouchableNativeFeedback>
    </View>
  );
};

const BrandHeader = () => {
  return (
    <View>
      <Image
        source={require('../../assets/logo_shape.png')}
        style={styles.logo}
      />
      <Text style={styles.note}>{strings.loginNote}</Text>
    </View>
  );
};

const renderScene = SceneMap({
  first: FirstRoute,
  second: SecondRoute,
});

const Login = ({navigation}) => {
  const layout = useWindowDimensions();

  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    {key: 'first', title: 'First', navigation: navigation},
    {key: 'second', title: 'Second', navigation: navigation},
  ]);

  return (
    <View style={{width: dimensions.maxWidth, height: '100%'}}>
      <TabView
        navigationState={{index, routes}}
        renderScene={renderScene}
        onIndexChange={setIndex}
        initialLayout={layout}
        renderTabBar={() => null}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  logo: {
    width: 80,
    height: 80,
    alignSelf: 'center',
    marginTop: 30,
    //borderWidth: 1,
    //borderColor: colors.textSecondary,
    //borderRadius: 12,
  },
  note: {
    fontSize: dimensions.bigFontSize,
    color: colors.textPrimary,
    alignSelf: 'center',
    marginTop: dimensions.standardMargin,
    marginBottom: 30,
  },
  tabContainer: {
    flex: 1,
    padding: dimensions.bigPadding,
    margin: dimensions.bigMargin,
  },
  loginButton: {
    marginTop: 2 * dimensions.bigMargin,
  },
  bottomText: {
    textAlign: 'right',
    marginTop: dimensions.bigMargin,
    padding: dimensions.standardPadding,
  },
  textFieldContainer: {
    marginTop: dimensions.standardMargin,
  },
});
export default Login;
