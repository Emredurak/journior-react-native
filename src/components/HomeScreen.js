import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Feed from '../Main Tabs/Feed';
import Browse from '../Main Tabs/Browse';
import NewPost from '../Main Tabs/NewPost';
import {
  faHome,
  faThLarge,
  faPlusSquare,
} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import colors from '../styles/colors';

const Tab = createBottomTabNavigator();

const HomeScreen = () => {
  return (
    <Tab.Navigator
      initialRouteName="Feed"
      tabBarOptions={{
        activeTintColor: colors.textPrimary,
        inactiveTintColor: colors.textSecondary,
      }}>
      <Tab.Screen
        name="Feed"
        component={Feed}
        listeners={({navigation}) => ({
          tabPress: e => {
            e.preventDefault();
            if (navigation.isFocused()) {
              navigation.navigate('Feed', {scrollToTop: true});
            } else {
              navigation.navigate('Feed', {scrollToTop: false});
            }
          },
        })}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({color, focused}) => {
            let size = focused ? 20 : 16;
            return <FontAwesomeIcon icon={faHome} color={color} size={size} />;
          },
        }}
      />
      <Tab.Screen
        name="Create"
        component={NewPost}
        options={{
          tabBarLabel: 'Create',
          tabBarIcon: ({color, focused}) => {
            let size = focused ? 20 : 16;
            return (
              <FontAwesomeIcon icon={faPlusSquare} color={color} size={size} />
            );
          },
        }}
      />
      <Tab.Screen
        name="Browse"
        component={Browse}
        options={{
          tabBarLabel: 'Browse',
          tabBarIcon: ({color, focused}) => {
            let size = focused ? 20 : 16;
            return (
              <FontAwesomeIcon icon={faThLarge} color={color} size={size} />
            );
          },
        }}
      />
    </Tab.Navigator>
  );
};

export default HomeScreen;
