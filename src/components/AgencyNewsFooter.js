import React, {useRef, useState, memo} from 'react';
import {
  Animated,
  Image,
  Text,
  TouchableOpacity,
  View,
  StyleSheet,
} from 'react-native';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faHeart} from '@fortawesome/free-solid-svg-icons';
import {faHeart as faHeartOutline} from '@fortawesome/free-regular-svg-icons';
import colors from '../styles/colors';
import dimensions from '../styles/dimensions';

const AgencyNewsFooter = ({
  liked,
  likeCount,
  category,
  prettyDate,
  ownerImgSrc,
  ownerName,
}) => {
  const [isLiked, setLiked] = useState(liked);
  const likeAnimation = useRef(new Animated.Value(1)).current;
  const startLikeAnimation = () => {
    // Will change fadeAnim value to 1 in 5 seconds
    Animated.timing(likeAnimation, {
      toValue: 0.1,
      duration: 1,
      useNativeDriver: true,
    }).start(() => {
      /* completion callback */
      setLiked(!isLiked);
      finishLikeAnimation();
    });
  };
  const finishLikeAnimation = () => {
    // Will change fadeAnim value to 0 in 3 seconds
    Animated.timing(likeAnimation, {
      toValue: 1,
      duration: 250,
      useNativeDriver: true,
    }).start(() => {
      /* completion callback */
    });
  };

  return (
    <View style={styles.footer}>
      <View style={styles.owner}>
        <Image
          source={{uri: 'https://reactjs.org/logo-og.png'}}
          style={styles.ownerIcon}
        />
        <Text numberOfLines={2} ellipsizeMode="tail" style={styles.ownerName}>
          New York Times
        </Text>
      </View>
      <View style={styles.details}>
        <View style={styles.dateSection}>
          <Text style={styles.category}>Breaking</Text>
          <Text style={styles.date}>2 hours ago</Text>
        </View>

        <View style={styles.likesSection}>
          <TouchableOpacity onPress={startLikeAnimation}>
            <Animated.View
              style={[
                styles.likesSection,
                {transform: [{scale: likeAnimation}]},
              ]}>
              <FontAwesomeIcon
                icon={isLiked ? faHeart : faHeartOutline}
                style={styles.likeIcon}
                size={24}
                color={isLiked ? colors.accentBurgundy : colors.textPrimary}
              />
            </Animated.View>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  footer: {
    flexDirection: 'row',
    paddingHorizontal: dimensions.standardPadding,
    paddingVertical: dimensions.xxShortPadding,
    paddingRight: dimensions.bigPadding,
  },
  owner: {
    flexDirection: 'row',
    flex: 5,
  },
  details: {
    flexDirection: 'row',
    flex: 5,
    justifyContent: 'space-between',
    borderLeftWidth: 1,
    borderColor: colors.separatorColor,
  },
  ownerIcon: {
    borderRadius: 30,
    width: 30,
    height: 30,
    alignSelf: 'center',
  },
  ownerName: {
    fontSize: dimensions.shortFontSize,
    textAlignVertical: 'center',
    flexShrink: 1,
    padding: dimensions.xxShortPadding,
  },
  dateSection: {
    alignItems: 'center',
    alignSelf: 'center',
    marginLeft: dimensions.standardMargin,
  },
  category: {
    fontSize: dimensions.xShortFontSize,
  },
  date: {
    fontSize: dimensions.xxShortFontSize,
    color: colors.textSecondary,
  },
  likesSection: {
    alignItems: 'center',
    alignSelf: 'center',
  },
  likeIcon: {
    color: colors.textSecondary,
  },
});

export default memo(AgencyNewsFooter);
