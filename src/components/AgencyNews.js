import React, {memo} from 'react';
import {Text, StyleSheet, View, Image, TouchableOpacity} from 'react-native';
import dimensions from '../styles/dimensions';
import colors from '../styles/colors';
import {faEllipsisV} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import AgencyNewsFooter from './AgencyNewsFooter';
import app_variables from '../styles/app_variables';

const AgencyNews = ({header, subheader, src, liked, newsUrl, navigation}) => {
  function goToNews() {
    navigation.navigate('NewsWebView', {src: newsUrl});
  }

  return (
    <View style={styles.container}>
      <TouchableOpacity activeOpacity={1} onPress={goToNews}>
        <View style={styles.header}>
          <Text style={app_variables.title}>{header}</Text>
          <TouchableOpacity style={app_variables.showMore}>
            <FontAwesomeIcon
              icon={faEllipsisV}
              color={colors.textPrimary}
              size={16}
            />
          </TouchableOpacity>
        </View>
        <View style={styles.body}>
          <Image source={{uri: src}} style={styles.newsImage} />
          <Text
            style={app_variables.subtitle}
            numberOfLines={dimensions.subtextMaxLine}
            ellipsizeMode="tail">
            {subheader}
          </Text>
        </View>
      </TouchableOpacity>
      <AgencyNewsFooter liked={liked} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.background,
  },
  header: {
    flexDirection: 'row',
    paddingHorizontal: dimensions.standardPadding,
    paddingVertical: dimensions.xShortPadding,
  },
  newsImage: {
    width: '100%',
    height: undefined,
    aspectRatio: 1.5,
    resizeMode: 'contain',
  },

  body: {
    padding: 0,
  },
});

export default memo(AgencyNews);
